package model;

/**
 * Created by Sandeep on 3/15/2018.
 */
import javax.persistence.*;

/**
 * Created by Sandeep on 12/21/2017.
 */
@Entity
public class org_experience {
    @Id
    @GeneratedValue
    private Long id;
    private String designation;

    private String company_name;
    private String duration;
    private String location;
    private String dates_employed;
    @Column(columnDefinition="LONGTEXT")
    private String data;
    @ManyToOne
    private Person person;

    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getDates_employed() {
        return dates_employed;
    }

    public void setDates_employed(String dates_employed) {
        this.dates_employed = dates_employed;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }


}

