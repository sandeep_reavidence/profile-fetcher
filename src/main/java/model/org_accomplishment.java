package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Sandeep on 4/2/2018.
 */
@Entity
public class org_accomplishment {
    @Id
    @GeneratedValue
    private Long id;
    private String type_of_accomplishment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType_of_accomplishment() {
        return type_of_accomplishment;
    }

    public void setType_of_accomplishment(String type_of_accomplishment) {
        this.type_of_accomplishment = type_of_accomplishment;
    }
}
