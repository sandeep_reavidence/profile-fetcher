package model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/2/2018.
 */
@Entity
public class org_certification {
    @Id
    @GeneratedValue
    private Long id;
    @Column(columnDefinition="LONGTEXT")
    private String name;
    private String date_of_issue;
    @Column(columnDefinition="LONGTEXT")
    private String certification_authority;
    @ManyToOne
    private org_accomplishment org_accomplishment;
    @ManyToOne
    private Person person;
    @ManyToOne
    private org_keycontacts org_key_contacts;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getOrg_key_contacts() {
        return org_key_contacts;
    }

    public void setOrg_key_contacts(org_keycontacts orgkeycontacts) {
        this.org_key_contacts = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate_of_issue() {
        return date_of_issue;
    }

    public void setDate_of_issue(String date_of_issue) {
        this.date_of_issue = date_of_issue;
    }

    public String getCertification_authority() {
        return certification_authority;
    }

    public void setCertification_authority(String certification_authority) {
        this.certification_authority = certification_authority;
    }

    public org_accomplishment getOrg_accomplishment() {
        return org_accomplishment;
    }

    public void setOrg_accomplishment(org_accomplishment org_accomplishment) {
        this.org_accomplishment = org_accomplishment;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
