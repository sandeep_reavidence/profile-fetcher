package model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/3/2018.
 */
@Entity
public class org_keycontacts {
    @Id
    @GeneratedValue
    private Long id;

    //@Column(name = "OrganizationID", columnDefinition = "TEXT")
    private String OrganizationID;

  /*  //@Column(name = "KeyContacts", columnDefinition = "TEXT")
    private String KeyContacts;

    //@Column(name = "TitleRole", columnDefinition = "TEXT")
    private String TitleRole;


   // @Column(name = "Address", columnDefinition = "TEXT")
    private String Address;

    //@Column(name = "Phone", columnDefinition = "TEXT")
    private String Phone;


    //@Column(name = "Fax", columnDefinition = "TEXT")
    private String Fax;

    //@Column(name = "Email", columnDefinition = "TEXT")
    private String Email;

    //@Column(name = "KeyContactsURL", columnDefinition = "TEXT")
    private String KeyContactsURL;


    //@Column(name = "OtherURLs", columnDefinition = "TEXT")
    private String OtherURLs;


    //@Column(name = "HealthcareDegree", columnDefinition = "TEXT")
    private String HealthcareDegree;

    //@Column(name = "PTCommitteeMember", columnDefinition = "TEXT")
    private String PTCommitteeMember;

    //@Column(name = "Expertise", columnDefinition = "TEXT")
    private String Expertise;
*/

    //@Column(name = "AcademicAffiliations", columnDefinition = "TEXT")
   // private String AcademicAffiliations;


    //@Column(name = "PreviousEmployerAffiliations", columnDefinition = "TEXT")
   /* private String PreviousEmployerAffiliations;

    //@Column(name = "KeyCustomerAttachments", columnDefinition = "TEXT")
    private String KeyCustomerAttachments;

    //@Column(name = "CustomerInteractions", columnDefinition = "TEXT")
    private String CustomerInteractions;
*/

    //@Column(name = "linked_in_profile", columnDefinition = "TEXT")
    private String LinkedInProfile;


   /* //@Column(name = "photo", columnDefinition = "TEXT")
    private String photo;


    *//**
     *//*

    //@Column(name = "CreateDate")
    Calendar CreateDate;
    *//**
     *//*

    //@Column(name = "ModifyDate")
    Calendar ModifyDate;

    //@Column(name = "SchoolAttended", columnDefinition = "TEXT")
    private String SchoolAttended;

    //@Column(name = "Summary", columnDefinition = "LONGTEXT")
    private String Summary;

    //@Column(name = "Publication", columnDefinition = "TEXT")
    @Basic(fetch = FetchType.EAGER)
    private String Publication;

    //@Column(name = "HonorsAwards", columnDefinition = "TEXT")
    @Basic(fetch = FetchType.EAGER)
    private String HonorsAwards;

   // @Column(name = "Projects", columnDefinition = "TEXT")
    @Basic(fetch = FetchType.EAGER)
    private String Projects;


    //@Column(name = "r_status", insertable = false)
    @Basic(fetch = FetchType.EAGER)
    Integer r_status;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(String organizationId) {
        this.OrganizationID = organizationId;
    }

   /* public String getKeyContacts() {
        return KeyContacts;
    }

    public void setKeyContacts(String keyContacts) {
        this.KeyContacts = keyContacts;
    }

    public String getTitleRole() {
        return TitleRole;
    }

    public void setTitleRole(String titleRole) {
        this.TitleRole = titleRole;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        this.Address = address;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        this.Fax = fax;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public String getKeyContactsURL() {
        return KeyContactsURL;
    }

    public void setKeyContactsURL(String keyContactsURL) {
        this.KeyContactsURL = keyContactsURL;
    }

    public String getOtherURLs() {
        return OtherURLs;
    }

    public void setOtherURLs(String otherURLs) {
        this.OtherURLs = otherURLs;
    }

    public String getHealthcareDegree() {
        return HealthcareDegree;
    }

    public void setHealthcareDegree(String healthcareDegree) {
        this.HealthcareDegree = healthcareDegree;
    }

    public String getPTCommitteeMember() {
        return PTCommitteeMember;
    }

    public void setPTCommitteeMember(String PTCommitteeMember) {
        this.PTCommitteeMember = PTCommitteeMember;
    }

    public String getExpertise() {
        return Expertise;
    }

    public void setExpertise(String expertise) {
        this.Expertise = expertise;
    }*/

   /* public String getAcademicAffiliations() {
        return AcademicAffiliations;
    }

    public void setAcademicAffiliations(String academicAffiliations) {
        this.AcademicAffiliations = academicAffiliations;
    }*/

   /* public String getPreviousEmployerAffiliations() {
        return PreviousEmployerAffiliations;
    }

    public void setPreviousEmployerAffiliations(String previousEmployerAffiliations) {
        this.PreviousEmployerAffiliations = previousEmployerAffiliations;
    }

    public String getKeyCustomerAttachments() {
        return KeyCustomerAttachments;
    }

    public void setKeyCustomerAttachments(String keyCustomerAttachments) {
        this.KeyCustomerAttachments = keyCustomerAttachments;
    }

    public String getCustomerInteractions() {
        return CustomerInteractions;
    }

    public void setCustomerInteractions(String customerInteractions) {
        this.CustomerInteractions = customerInteractions;
    }*/

    public String getLinkedInProfile() {
        return LinkedInProfile;
    }

    public void setLinkedInProfile(String linked_in_profile) {
        this.LinkedInProfile = linked_in_profile;
    }

  /*  public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Calendar getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.CreateDate = createDate;
    }

    public Calendar getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(Calendar modifyDate) {
        this.ModifyDate = modifyDate;
    }

    public String getSchoolAttended() {
        return SchoolAttended;
    }

    public void setSchoolAttended(String schoolAttended) {
        this.SchoolAttended = schoolAttended;
    }

    public String getSummary() {
        return Summary;
    }

    public void setSummary(String org_summary) {
        this.Summary = org_summary;
    }

    public String getPublication() {
        return Publication;
    }

    public void setPublication(String org_publication) {
        this.Publication = org_publication;
    }

    public String getHonorsAwards() {
        return HonorsAwards;
    }

    public void setHonorsAwards(String honorsAwards) {
        this.HonorsAwards = honorsAwards;
    }

    public String getProjects() {
        return Projects;
    }

    public void setProjects(String projects) {
        this.Projects = projects;
    }

    public Integer getrR_status() {
        return r_status;
    }

    public void setrR_status(Integer rStatus) {
        this.r_status = rStatus;
    }*/
}

