package reavidence;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import model.Person;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;

public class ProfileFetcherTasks {

    public ProfileFetcherTasks(int type){
        System.out.println("Hello World after 30 secs");
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ProfileFetcherTasks(ArrayList<String> links) {
        Logger LOG = (Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        LOG.setLevel(Level.WARN);

        ArrayList<String> urlList = new ArrayList<String>();
       /* urlList.add("https://www.linkedin.com/in/christina-tolomeo-278b047/");
        urlList.add("https://www.linkedin.com/in/ACoAAADquDwBXw78SFvBrWKrQemdBhrRhHC0EQs/");
        urlList.add("https://www.linkedin.com/in/ACoAABqp7EIBQP607MHhyXbMGJI58xu1DNCoFcM/");
        urlList.add("https://www.linkedin.com/in/ACoAAAiPr2QBxwJ4q__zPgt4TxINBQn5Uud_k6U/");
       urlList.add("https://www.linkedin.com/in/ACoAABm8LDYBAhplgyp7tT_XuiNQWo70kCw58g0/");
       urlList.add("https://www.linkedin.com/in/ACoAABSraGEBq-wV1C39vmttZsVqldBrkshD7BA/");*/
      urlList.add("https://www.linkedin.com/in/shaileshk/");
      //urlList.add("https://www.linkedin.com/in/almahfoudhma/");
        //urlList.add("https://www.linkedin.com/in/gopuri/");

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Downloads\\chromedriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        Operations utils = new Operations();
        //Login into linked in.
        utils.performLogin(driver, 1);

        for (String url : links) {
            driver.navigate().to(url);
            utils.sleepNSeconds(3);

            //Scroll till ending to load all the data.
            ((JavascriptExecutor) driver)
                    .executeScript("window.scrollTo(0, document.body.scrollHeight)");

            utils.sleepNSeconds(3);

            //TODO uncomment
            /*if (driver.findElements(By.cssSelector("div.presence-entity__image")).size() > 0) {
                WebElement imageButton = driver.findElement(By.cssSelector("div.presence-entity__image"));
               utils.extractImage(driver,url,imageButton);

            }*/


            //Get Description Profile Summary.

            if (driver.findElements(By.className("pv-top-card-section__summary")).size() > 0) {
                System.out.println("Summary:==========================");
                WebElement summary = driver.findElement(By.className("pv-top-card-section__summary"));
                utils.extractSummaryFields(summary,driver,url);
                }

            //Check if org_experience is present.
            //TODO initialize experience section ID.
            System.out.println("+++++++++++++++++++++++++++++++++"+driver.findElements(By.id("experience-section")).size()+"+++++++++++++++++++++");
            if (driver.findElements(By.id("experience-section")).size() > 0) {
                System.out.println("org_experience:==========================");
                WebElement experience = driver.findElement(By.id("experience-section"));
                utils.extractExperience(experience, driver,url);
            }

            if (driver.findElements(By.className("volunteering-section")).size() > 0) {
                System.out.println("Volunteer org_experience:==========================");
                WebElement experience = driver.findElement(By.className("volunteering-section"));
                utils.extractVolExperience(experience, driver,url);
            }

            //Check if Education is present.
            if (driver.findElements(By.id("education-section")).size() > 0) {
                System.out.println("Education:==========================");
                WebElement educationSection = driver.findElement(By.id("education-section"));
                utils.extractEducation(educationSection, driver,url);
            }

            //Check if Skills and Endorsements are present.
            //TODO initialize a skills classname string.
            if (driver.findElements(By.className("pv-featured-skills-section")).size() > 0) {
                System.out.println("Skills:==========================");
                WebElement skills = driver.findElement(By.className("pv-featured-skills-section"));
                utils.extractSkills(skills, driver, 1,url);

            }else if (driver.findElements(By.className("pv-org_skill-categories-section")).size() > 0){
                System.out.println("Skills:==========================");
                WebElement skillsSection = driver.findElement(By.className("pv-org_skill-categories-section"));
                utils.extractSkills(skillsSection, driver, 2,url);
            }else if(driver.findElements(By.className("pv-skill-categories-section")).size() > 0){
                WebElement skillsSection = driver.findElement(By.className("pv-skill-categories-section"));
                utils.extractSkills(skillsSection, driver, 2,url);
            }



            utils.sleepNSeconds(3);

            //Check if Accomplishment is present.
            if (driver.findElements(By.className("pv-accomplishments-section")).size() > 0) {
                List<WebElement> a=driver.findElements(By.cssSelector("button.pv-accomplishments-block__expand")) ;

                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.publications")).size() > 0) {
                    System.out.println("publications:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.publications"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url);
                }

                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.projects")).size() > 0) {
                    System.out.println("projects:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.projects"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url);
                }
                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.certifications")).size() > 0) {
                    System.out.println("certifications:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.certifications"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url);
                }
                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.honors")).size() > 0) {
                    System.out.println("Honors&award:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.honors"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url);

                }
            }

        }

    }


}
