package reavidence;

import org.hibernate.Session;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Operations {

    ParseAndSaveText parseAndSaveText=new ParseAndSaveText();
    String link="";
    public void performLogin(WebDriver driver, int loginAccount){
        driver.get("https://www.linkedin.com/");

        if(loginAccount == 1){
            driver.findElement(By.id("login-email")).sendKeys("mariojosephmathew@gmail.com");
            driver.findElement(By.id("login-password")).sendKeys("hereiam123");
        }

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("login-submit")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


    }

    public void sleepNSeconds(int n){
        try {
            Thread.sleep(n * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void openShowMoreButton(WebElement webElement, WebDriver driver){

        if (webElement.findElements(By.className("pv-profile-section__see-more-inline")).size() > 0) {
            WebElement button = webElement.findElement(By.className("pv-profile-section__see-more-inline"));
            if (button != null) {
                Actions actions = new Actions(driver);
                actions.moveToElement(button).click().perform();
            }
        } else if(webElement.findElements(By.cssSelector("button.pv-profile-section__see-more-inline.link")).size()>0){
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -70);", webElement);
            System.out.println("+++++++++++++++++++++++++++++++++++++++found show more+++++++++++++++++++++++++++");
            WebElement element =webElement.findElement(By.cssSelector("button.pv-profile-section__see-more-inline.link"));
            if(element != null){
                Actions actions = new Actions(driver);
                actions.moveToElement(element).click().perform();
            }
        }
        sleepNSeconds(3);
    }

    public void openShowMoreButtonSkills(WebElement webElement, WebDriver driver){
         if (webElement.findElements(By.xpath("//button/span[starts-with(text(), 'Show ')]")).size() > 0) {
            WebElement element = webElement.findElement(By.xpath("//button/span[starts-with(text(), 'Show ')]"));
            if (element != null) {
                Actions actions = new Actions(driver);
                actions.moveToElement(element).click().perform();
            }
        }
    }
    void extractSummaryFields(WebElement summary,WebDriver driver,String url){
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(summary).perform();
        String name="";
        String head_line="";
        String location="";
        if(driver.findElements(By.className("pv-top-card-section__name")).size() > 0)
            System.out.println("Name: " + driver.findElement(By.className("pv-top-card-section__name")).getText());
        name=driver.findElement(By.className("pv-top-card-section__name")).getText();
        if(driver.findElements(By.className("pv-top-card-section__headline")).size() > 0)
            System.out.println("Headline: "+ driver.findElement(By.className("pv-top-card-section__headline")).getText());
            head_line=driver.findElement(By.className("pv-top-card-section__headline")).getText();
        if(driver.findElements(By.className("pv-top-card-section__location")).size() > 0)
            System.out.println("Location: "+ driver.findElement(By.className("pv-top-card-section__location")).getText());
            location=driver.findElement(By.className("pv-top-card-section__location")).getText();
            parseAndSaveText.saveSummary(name,head_line,location,url);
    }

    public void extractExperience(WebElement experienceSection, WebDriver driver,String url) {
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(experienceSection).perform();
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", experienceSection);
        openShowMoreButton(experienceSection, driver);
        parseAndSaveText.extractExperience(experienceSection.getText(),url);
        System.out.println(experienceSection.getText());

    }


    public void extractVolExperience(WebElement experienceSection, WebDriver driver,String url) {
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(experienceSection).perform();
        parseAndSaveText.extractVolExperience(experienceSection.getText(),url);
        System.out.println(experienceSection.getText());

    }
    public void extractEducation(WebElement educationSection, WebDriver driver,String url) {
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(educationSection).perform();
        openShowMoreButton(educationSection,driver);
        parseAndSaveText.extractStudy(educationSection.getText(),url);
        System.out.println(educationSection.getText());
    }

    public void extractSkills(WebElement skillsSection, WebDriver driver, int skillsClassType,String url){
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(skillsSection).perform();
        openShowMoreButtonSkills(skillsSection, driver);
        System.out.println("From Skills.");
        if(skillsClassType == 1){
            List<WebElement> allElements = skillsSection.findElements(By.className("pv-org_skill-category-entity__name"));
            parseAndSaveText.fetchSkill(allElements,url);
            for(WebElement skill : allElements){
                System.out.println(skill.getText());
            }
        }else if(skillsClassType == 2){

            List<WebElement> allElements = skillsSection.findElements(By.className("pv-skill-category-entity__name "));
            parseAndSaveText.fetchSkill(allElements,url);
            for(WebElement skill : allElements){
                System.out.println(skill.getText());
            }

        }

    }

    public void extractAccomplishment(WebElement accomplishmentSection, WebDriver driver,String url) {
        //Actions scrollToElement = new Actions(driver);
        //scrollToElement.moveToElement(accomplishmentSection).perform();
        //WebElement element = driver.findElement(By.className("pv-accomplishments-section"));
        /*Actions actions = new Actions(driver);
        actions.moveToElement(accomplishmentSection, 0, -70);
        actions.perform();*/
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -200);", accomplishmentSection);

        openAccomplistmentButton(accomplishmentSection,driver);
        sleepNSeconds(2);
        openShowMoreButton(accomplishmentSection, driver);
        System.out.println(accomplishmentSection.getText());
        parseAndSaveText.fetchCertification(accomplishmentSection.getText(),url);
    }

    public void openAccomplistmentButton(WebElement webElement, WebDriver driver){
        if (webElement.findElements(By.className("pv-accomplishments-block__expand")).size() > 0) {
            System.out.println("Button exists! found!");
            WebElement button = webElement.findElement(By.className("pv-accomplishments-block__expand"));

            Actions builder = new Actions(driver);
            builder.moveToElement( button).click( button );
            builder.perform();
            //button.click();
        }
        sleepNSeconds(3);
    }

    public void extractImage(WebDriver driver,String link,WebElement imageButton) {
        Actions scrollToElement = new Actions(driver);
        scrollToElement.moveToElement(imageButton).perform();
        imageButton.click();
        try {
            String s = driver.findElement(By.cssSelector("img.pv-member-photo-modal__image")).getAttribute("src");
            URL url = new URL(s);
            System.out.println(url);
            BufferedImage bufImgOne = ImageIO.read(url);
            String name_url=link.substring(28,67);
            System.out.println(name_url);
            ImageIO.write(bufImgOne, "png", new File("D:\\testlinkedin\\"+name_url+".png"));
            sleepNSeconds(3);
            driver.findElement(By.cssSelector("button.pv-member-photo-modal__close")).click();
        }catch (NoSuchElementException | StaleElementReferenceException e){} catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
