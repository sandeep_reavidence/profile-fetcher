package reavidence;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Sandeep on 3/19/2018.
 */
public class FetchPerson {


    public void FetchPersonTasks() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Downloads\\chromedriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        Operations utils = new Operations();
        utils.performLogin(driver, 1);
        utils.sleepNSeconds(3);
        driver.findElement(By.xpath("/html/body/nav/div/form/div/div/div/artdeco-typeahead/artdeco-typeahead-input/input")).sendKeys("Humana",Keys.ENTER);
       // search-filters-bar__all-filters button-tertiary-medium-muted mr3
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("button.search-filters-bar__all-filters.button-tertiary-medium-muted.mr3")).click();
        driver.findElement(By.cssSelector("button.search-filters-bar__all-filters.button-tertiary-medium-muted.mr3")).click();
        driver.findElement(By.id("search-advanced-title")).sendKeys("Medical Director");
        WebElement location = driver.findElement(By.xpath("//*[contains(text(), 'Locations')]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", location);
        utils.sleepNSeconds(2);

        driver.findElement(By.xpath("/html/body/div[1]/artdeco-modal-overlay/artdeco-modal/artdeco-modal-content/div/div[1]/ul/li[5]/form/fieldset/ol/li[2]/label")).click();
        driver.findElement(By.xpath("/html/body/div[1]/artdeco-modal-overlay/artdeco-modal/artdeco-modal-content/div/div[1]/ul/li[6]/form/fieldset/ol/li[2]/label")).click();
        driver.findElement(By.xpath("/html/body/div[1]/artdeco-modal-overlay/artdeco-modal/artdeco-modal-content/div/div[1]/ul/li[8]/form/fieldset/ol/li[2]/label")).click();
        driver.findElement(By.xpath("/html/body/div[1]/artdeco-modal-overlay/artdeco-modal/artdeco-modal-content/div/div[1]/ul/li[8]/form/fieldset/ol/li[3]/label")).click();
        driver.findElement(By.xpath("/html/body/div[1]/artdeco-modal-overlay/artdeco-modal/artdeco-modal-content/div/div[1]/ul/li[8]/form/fieldset/ol/li[5]/label")).click();
        driver.findElement(By.cssSelector("button.search-advanced-facets__button--apply.button-primary-large")).click();
        //driver.navigate().to("https://www.linkedin.com/search/results/people/?company=&facetCurrentCompany=%5B%224257%22%5D&facetGeoRegion=%5B%22us%3A0%22%5D&facetIndustry=%5B%2242%22%2C%2214%22%2C%22124%22%5D&firstName=&keywords=Humana&lastName=&origin=FACETED_SEARCH&school=&title=Medical%20Director");
        utils.sleepNSeconds(4);



        WebElement total_size = driver.findElement(By.cssSelector("h3.search-results__total"));
        int value = Integer.parseInt(total_size.getText().replaceAll("[^0-9]", ""));
        System.out.println(value/10);
        int page_size=0;

while(page_size<=value/10) {
    WebElement industries = driver.findElement(By.cssSelector("ul.results-list.ember-view"));

    List<WebElement> links = industries.findElements(By.tagName("li"));
    for (WebElement element : links) {
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        if (element.findElements(By.tagName("a")).size() > 0) {
            WebElement webElement = element.findElement(By.tagName("a"));
            WebElement name = element.findElement(By.cssSelector(".search-result--person .actor-name, .search-result--provider .actor-name"));
            String link = webElement.getAttribute("href");
            if (link.length() < 70) {
                System.out.println(link);
            }
        }
    }

    driver.findElement(By.cssSelector("button.next")).click();

    utils.sleepNSeconds(2);

}

    }

    public static void main(String[] args) {
        FetchPerson fetchPerson=new FetchPerson();
        try {
            fetchPerson.FetchPersonTasks();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
